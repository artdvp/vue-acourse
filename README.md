# VueJs Acourse.io

## Section 1

### ES6

- let, const
- arrow function
- class
- module
- destructuring

### Reactive Programming

> Data Flow + Propagation of Change
> Change Detection

```js
fucntion reactiveSetter (property, value) {
    this.$data[property] = value
    console.log(`${property} change to ${value}`)
}

function reactiveGetter (propperty){
    console.log(`get ${property}`)
    return this.$data[property]
}
```

---

## Section 2

### Component

> Registration

Global

```js
Vue.component('hello'{
    // options
})
```

Local

```js
const Hello = {
    // ootions
}

new Vue({
    // ...
    components: {
        Hello
    }
})
```

> Hello.vue

```html
<template>
    <div>
        Hi, {{ name }} <br/>
        <slot></slot>
    </div>
</template>

<style scoped>
    div {
        color: green;
    }
</style>

<script>
    export default {
        props: ['name']
    }
</script>
```

### Directives

- v-model
- v-text, v-html
- v-bind
- v-on
- v-if,v-else ,v-else-if , v-show
- v-for

App.vue

v-text

```html
<template>
  <div>
    <input v-model="name" type="text">
    <p v-text="name"></p>
  </div>
</template>

<script>
export default {
  data () {
    return {
      name: ''
    }
  }
}
</script>

<style>

</style>
```

```html
<template>
  <div>
    <input v-model="name" type="text">
    <p>{{ name }}</p>
  </div>
</template>

<script>
export default {
  data () {
    return {
      name: ''
    }
  }
}
</script>

<style>

</style>
```

v-if , v-show

```html
<template>
  <div>
    <input v-model="name" type="text">
    <p v-if="name">Hi, {{ name }}</p>
    <p v-show="name">Hi, {{ name }}</p>
  </div>
</template>

<script>
export default {
  data () {
    return {
      name: ''
    }
  }
}
</script>

<style>

</style>
```

แตกต่างกันอย่างไร

v-show → จะสร้าง Tag HTML เป็น display: none

v-if → หากไม่มีค่าจะลบจาก DOM → กรณีไม่ Render บ่อย

### v-else

```html
<template>
  <div>
    <input v-model="name" type="text">
    <p v-if="name">Hi, {{ name }}</p>
    <p v-else>No data</p>
    <p v-show="name">Hi, {{ name }}</p>
  </div>
</template>

<script>
export default {
  data () {
    return {
      name: ''
    }
  }
}
</script>

<style>

</style>
```

### v-else-if

```html
<template>
  <div>
    <input v-model="name" type="text">
    <p v-if="name && name !== 'artdvp'">Hi, {{ name }}</p>
    <p v-else-if="name ==='artdvp'">:-()</p>
    <p v-else>No data</p>
  </div>
</template>

<script>
export default {
  data () {
    return {
      name: ''
    }
  }
}
</script>

<style>

</style>
```

v-for

```html
<template>
  <div>
    <input v-model="name" type="text">
    <p v-if="name && name !== 'artdvp'">Hi, {{ name }}</p>
    <p v-else-if="name ==='artdvp'">:-()</p>
    <p v-else>No data</p>
    <p v-for="(x, index) in list">{{index}} : {{ x }}</p>
  </div>
</template>

<script>
export default {
  data () {
    return {
      name: '',
      list: [
        1, 2, 3, 4, 5
      ]
    }
  }
}
</script>

<style>

</style>
```

v-on

```html
<template>
  <div>
    <input v-model="name" type="text">
    <p v-if="name && name !== 'artdvp'">Hi, {{ name }}</p>
    <p v-else-if="name ==='artdvp'">:-()</p>
    <p v-else>No data</p>
    <p v-for="(x, index) in list">{{index}} : {{ x }}</p>
    <button v-on:click="sayHello">Hello</button>
  </div>
</template>

<script>
export default {
  data () {
    return {
      name: '',
      list: [
        1, 2, 3, 4, 5
      ]
    }
  },
  methods: {
    sayHello () {
      window.alert("Hi," + this.name)
    }
  }
}
</script>

<style>

</style>
```

```html
<template>
  <div>
    <input v-model="name" type="text">
    <p v-if="name && name !== 'artdvp'">Hi, {{ name }}</p>
    <p v-else-if="name ==='artdvp'">:-()</p>
    <p v-else>No data</p>
    <p v-for="(x, index) in list">{{index}} : {{ x }}</p>
    <button @click="sayHello">Hello</button>
  </div>
</template>

<script>
export default {
  data () {
    return {
      name: '',
      list: [
        1, 2, 3, 4, 5
      ]
    }
  },
  methods: {
    sayHello () {
      window.alert("Hi," + this.name)
    }
  }
}
</script>

<style>

</style>
```

```html
<template>
  <div>
    <input v-model="name" type="text">
    <p v-if="name && name !== 'artdvp'">Hi, {{ name }}</p>
    <p v-else-if="name ==='artdvp'">:-()</p>
    <p v-else>No data</p>
    <p v-for="(x, index) in list">{{index}} : {{ x }}</p>
    <button @click="sayHello(123)">Hello</button>
  </div>
</template>

<script>
export default {
  data () {
    return {
      name: '',
      list: [
        1, 2, 3, 4, 5
      ]
    }
  },
  methods: {
    sayHello (value) {
      window.alert("Hi," + this.name + " " + value)
    }
  }
}
</script>

<style>

</style>
```

### Component

component/HelloWorld.vue

```html
<template>
  <div>
    <h3>Hi, {{ name }}</h3> 
  </div>
</template>

<script>
export default {
  props: ['name']
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>

</style>

```

App.vue

```html
<template>
  <div>
    <input v-model="name" type="text">
    <hello name="artdvp"></hello>
  </div>
</template>

<script>
import Hello from './components/HelloWorld'

export default {
  components: {
    Hello
  },
  data () {
    return {
      name: '',
      list: [
        1, 2, 3, 4, 5
      ]
    }
  },
  methods: {
    sayHello (value) {
      window.alert("Hi," + this.name + " " + value)
    }
  }
}
</script>

<style>

</style>
```

new App.vue

```html
<template>
  <div>
    <input v-model="name" type="text">
    <hello v-bind:name="name"></hello>
  </div>
</template>

<script>
import Hello from './components/HelloWorld'

export default {
  components: {
    Hello
  },
  data () {
    return {
      name: '',
      list: [
        1, 2, 3, 4, 5
      ]
    }
  },
  methods: {
    sayHello (value) {
      window.alert("Hi," + this.name + " " + value)
    }
  }
}
</script>

<style>

</style>
```

v-bind

```html
<template>
  <div>
    <input v-model="name" type="text">
    <!-- <hello v-bind:name="name"></hello> -->
    <hello :name="name"></hello> 
    <hello :name="1"></hello> <!-- จะเป็น number -->
    <hello name="1"></hello> <!-- จะเป็น string -->
  </div>
</template>

<script>
import Hello from './components/HelloWorld'

export default {
  components: {
    Hello
  },
  data () {
    return {
      name: '',
      list: [
        1, 2, 3, 4, 5
      ]
    }
  },
  methods: {
    sayHello (value) {
      window.alert("Hi," + this.name + " " + value)
    }
  }
}
</script>

<style>

</style>
```

---

### Components Structure

> template

if use runtime render


```js
//main.js
import Vue from 'vue'
import App from './App'

new Vue({
    el: '#app',
    render (h) {
        return h(App)
    }
})
```

> data

> Vue.nectTick

- Call after next DOM update cycle
- Wait for DOM update

```js
this.rating = 5
// DOM not updateed yet
this.$nexxtTick(() =>{
    // DOM updated
    $(this.$refs.rating).rating()
})
```

## Life cycle

use jquery in nextTick

## Add jquery

npm install jquery --save-dev

```js
// webpack
const webpack = require('webpack')
...
 plugins: [
    new webpack.ProvidePlugin({
      'window.$': 'jquery',
      '$': 'jquery',
      'jQuery': 'jquery'
    })

```

install script-loader

```js
npm install script-loader
```

```
npm install --save semantic-ui
```