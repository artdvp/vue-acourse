// reactive 2

function createReactiveObject (obj) {
    const keys = Object.keys(obj)
    obj.$data = Object.assign({}, obj)
    keys.forEach((key) => {
        Object.defineProperty(obj,key , {
            get: reactiveGetter.bind(obj, key),
            set: reactiveSetter.bind(obj,key)
        })
    })
}

function reactiveSetter(property, value){
    this.$data[property] = value
    console.log(`${property} changed tp ${value}`)
}

function reactiveGetter(property) {
    console.log(`get ${property}`)
    return this.$data[property]
}

let user = {
    name:'',
    score: 0
}

createReactiveObject(user)

user.name = 'vue'
console.log(user.score)