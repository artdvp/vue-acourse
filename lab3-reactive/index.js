// Reactive

let user = {}

// 
// write
Object.defineProperty(user, 'name',{
    get () {
        return this.$name
    },
    set (value) {
        this.$name = value
        console.log(`name changed to ${value}`)
    }
})
//

user.name = 'art'

let user2 = {
    get name () {
        return this.$name
    },
    set name (value) {
        this.$name = value
        console.log(`name changed2 to ${value}`)
    }
}

user2.name = 'art'

// ---------------------------------------

function createReactiveObject(obj){
    const keys = Object.keys(obj)
    console.log(keys)
}

let user3 = {
    name: '',
    score: 0
}

createReactiveObject(user3)

user3.name = 'dvp'