
// arrow function in object

const obj = {
    a:1,
    b:2,
    sum: function(){
        return this.a + this.b
    }
}

console.log(obj.sum());

// -----------------------------

const obj2 = {
    a:11,
    b:22,
    sum () {
        return this.a + this.b
    }
}

console.log(obj2.sum());