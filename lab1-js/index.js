console.log('Hello Vue.js');

// scope
var a = 10;

console.log(a);

if(true) {
    var a = 20;
    console.log(a);
}

console.log(a);

///
console.log('------------')


if(true) {
    var a = 50;
    console.log(a);
}

// const

const b = 40;

// arrow function
const c = () => {
    console.log('hhhhh');
}

c();

// -----------------------------

setTimeout(function() {
    console.log('gggHH');
},1000);

setTimeout(() => {
    console.log('GGG');
},2000)