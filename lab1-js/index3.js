// class Javascript

function Person (name) {
    this.name = name
    this.talk = function () {
        console.log('Hi, ' + this.name)
    }
}


const aa = new Person('dvp')
aa.talk()

function Person2 (name) {
    this.name = name
    this.talk = function () {
        console.log('Hii, ' + this.name)
    }
}


Person2.prototype.talk = function () {
    console.log('Hi, '+ this.name)
}

const bb = new Person2('RT')
bb.talk()


// ES6

class Person3 {
    constructor (name) {
        this.name = name
    }
    talk () {
        console.log('Hi, ' + this.name)
    }
}

const ccc = new Person3('KKK')
ccc.talk()